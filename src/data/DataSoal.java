package data;

import java.awt.GridLayout;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import soal.Soal;


public class DataSoal extends DataUpdate{
    
    private static ArrayList<Soal> arraySoal = new ArrayList<>();
    private int index = 0;
    
    JButton nextDataSoal = new JButton("Soal Selanjutnya");
    JButton previousDataSoal = new JButton("Soal Sebelumnya");
                
    JLabel question = new JLabel();
    JLabel jawaban_1 = new JLabel();
    JLabel jawaban_2 = new JLabel(); 
    JLabel jawaban_3 = new JLabel();
    JLabel jawaban_4 = new JLabel();
    JLabel jawaban_5 = new JLabel();
    JLabel jawabanBenar = new JLabel();
    
    JTextField inputQuestion = new JTextField();
    JTextField inputJawaban_1 = new JTextField();
    JTextField inputJawaban_2 = new JTextField(); 
    JTextField inputJawaban_3 = new JTextField();
    JTextField inputJawaban_4 = new JTextField();
    JTextField inputJawaban_5 = new JTextField();
    JTextField inputJawabanBenar = new JTextField();

    JButton nextSoal = new JButton("Soal Selanjutnya");
    JButton previousSoal = new JButton("Soal Sebelumnya");
    
    public DataSoal(){
        nextSoal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if (index != getArraySoal().size() - 1 ){
                    index = index + 1;

                    question.setText(Integer.toString(index+1) +". "+ getArrayIndex(index).getQuestion() );

                    jawabanBenar.setText( "    Jawaban benar: " + getArrayIndex(index).getJawabanBenar());

                    jawaban_1.setText( "    a. " + getArrayIndex(index).getJawaban_1() ) ;
                    jawaban_2.setText( "    b. " + getArrayIndex(index).getJawaban_2() ) ;

                    jawaban_3.setText( "    c. " + getArrayIndex(index).getJawaban_3() );
                    jawaban_4.setText( "    d. " + getArrayIndex(index).getJawaban_4() );
                    jawaban_5.setText( "    e. " + getArrayIndex(index).getJawaban_5() );

                }
            }
        });

        previousSoal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if (index != 0 ){
                    index = index - 1;

                    question.setText(Integer.toString(index+1) +". "+ getArrayIndex(index).getQuestion() );

                    jawabanBenar.setText( "    Jawaban benar: " + getArrayIndex(index).getJawabanBenar());

                    jawaban_1.setText( "    a. " + getArrayIndex(index).getJawaban_1() ) ;
                    jawaban_2.setText( "    b. " + getArrayIndex(index).getJawaban_2() ) ;

                    jawaban_3.setText( "    c. " + getArrayIndex(index).getJawaban_3() );
                    jawaban_4.setText( "    d. " + getArrayIndex(index).getJawaban_4() );
                    jawaban_5.setText( "    e. " + getArrayIndex(index).getJawaban_5() );

                }
            }
        });
    }
    
    public JPanel tambahUbahPanel(){
        JPanel inputSoalPanel = new JPanel(new GridLayout(7, 2, 5,5));
        
        inputQuestion.setText("");
        inputJawaban_1.setText("");
        inputJawaban_2.setText("");
        inputJawaban_3.setText("");
        inputJawaban_4.setText("");
        inputJawaban_5.setText("");
        
        inputSoalPanel.add(new JLabel("Pertanyaan: "));
        inputSoalPanel.add( inputQuestion );

        inputSoalPanel.add(new JLabel("Opsi Jawaban ( a. ): "));
        inputSoalPanel.add( inputJawaban_1 );
        
        inputSoalPanel.add(new JLabel("Opsi Jawaban ( b. ): "));
        inputSoalPanel.add( inputJawaban_2 );
        
        inputSoalPanel.add(new JLabel("Opsi Jawaban ( c. ): "));
        inputSoalPanel.add( inputJawaban_3 );
        
        inputSoalPanel.add(new JLabel("Opsi Jawaban ( d. ): "));
        inputSoalPanel.add( inputJawaban_4 );
        
        inputSoalPanel.add(new JLabel("Opsi Jawaban ( e. ): "));
        inputSoalPanel.add( inputJawaban_5 );
        
        inputSoalPanel.add(new JLabel("Jawaban Benar ( a / b / c / d / e ): "));
        
        return inputSoalPanel;
    }
    
    @Override
    public void tambahData() {
        
        Object[] options = { "a", "b", "c", "d", "e"};
        try{
            String tambahJawabanBenar = (String)JOptionPane.showInputDialog(null,tambahUbahPanel(), "Tambah Soal Tes", 
                JOptionPane.QUESTION_MESSAGE, null, options, "");
            
            if (    tambahJawabanBenar.equals("a") || tambahJawabanBenar.equals("b") || 
                    tambahJawabanBenar.equals("c") || tambahJawabanBenar.equals("d") || 
                    tambahJawabanBenar.equals("e") ){
                if ( inputQuestion.getText().isEmpty() ||
                    inputJawaban_1.getText().isEmpty() ||
                    inputJawaban_2.getText().isEmpty() ||
                    inputJawaban_3.getText().isEmpty() ||
                    inputJawaban_4.getText().isEmpty() ||
                    inputJawaban_5.getText().isEmpty() ){
                        JOptionPane.showMessageDialog(null, "Input tidak dapat kosong","Tambah Soal Tes", JOptionPane.WARNING_MESSAGE); 
                
            
                } else {
                    tambahData( inputQuestion.getText(),
                                inputJawaban_1.getText(),
                                inputJawaban_2.getText(),
                                inputJawaban_3.getText(), 
                                inputJawaban_4.getText(), 
                                inputJawaban_5.getText(), 
                                tambahJawabanBenar );
                    JOptionPane.showMessageDialog(null, "Penambahan data sukses", "Tambah Soal Tes", JOptionPane.INFORMATION_MESSAGE); 
                }
            }
        } catch(NullPointerException e){
        }
        
    }
    
    public static void tambahData(
            String question, String jawaban_1, String jawaban_2, 
            String jawaban_3, String jawaban_4, String jawaban_5, 
            String jawabanBenar) {    
        
        arraySoal.add( new Soal(
                question, jawaban_1, jawaban_2, 
                jawaban_3, jawaban_4, jawaban_5, 
                jawabanBenar
                )
        );
        
        float nilai = (float) (100.0 / getArraySoal().size());
        for (int i = 0; i < getArraySoal().size()   ; i++){
            getArrayIndex(i).setNilai(nilai);                    
        }
    }

    
    public static ArrayList<Soal> getArraySoal() {
        return arraySoal;
    }
    
    public static Soal getArrayIndex(int i) {
        return arraySoal.get(i);
    }
    
    public JPanel lihatSoalPanel(){
        JPanel daftarSoalPanel = new JPanel(new GridLayout(8, 2, 2, 5));
        
        index = 0;

        daftarSoalPanel.add( question );
        daftarSoalPanel.add(new JLabel(""));

        daftarSoalPanel.add( jawabanBenar );
        daftarSoalPanel.add(new JLabel("Nilai per soal: " + getArrayIndex(0).getNilai()  ));

        daftarSoalPanel.add( jawaban_1 );
        daftarSoalPanel.add(new JLabel(""));

        daftarSoalPanel.add( jawaban_2 );
        daftarSoalPanel.add(new JLabel(""));

        daftarSoalPanel.add( jawaban_3 );
        daftarSoalPanel.add(new JLabel(""));

        daftarSoalPanel.add( jawaban_4 );
        daftarSoalPanel.add(new JLabel(""));

        daftarSoalPanel.add( jawaban_5 );
        daftarSoalPanel.add(new JLabel(""));

        if ( getArraySoal().size() != 1 && !getArraySoal().isEmpty() ){
            daftarSoalPanel.add( previousSoal );
            daftarSoalPanel.add( nextSoal );
        }

        question.setText(Integer.toString(index+1) +". "+ getArrayIndex(index).getQuestion() );

        jawabanBenar.setText( "    Jawaban benar: " + getArrayIndex(index).getJawabanBenar());

        jawaban_1.setText( "    a. " + getArrayIndex(index).getJawaban_1() ) ;
        jawaban_2.setText( "    b. " + getArrayIndex(index).getJawaban_2() ) ;

        jawaban_3.setText( "    c. " + getArrayIndex(index).getJawaban_3() );
        jawaban_4.setText( "    d. " + getArrayIndex(index).getJawaban_4() );
        jawaban_5.setText( "    e. " + getArrayIndex(index).getJawaban_5() );

        return daftarSoalPanel;
    }
    
    @Override
    public void lihatData() {
        if ( !getArraySoal().isEmpty() ){
            
            JOptionPane.showMessageDialog(null, lihatSoalPanel(), "Daftar Soal Tes", JOptionPane.INFORMATION_MESSAGE);
            

        } else {
            JOptionPane.showMessageDialog(null, "Data Soal Kosong", "Daftar Soal Tes", JOptionPane.INFORMATION_MESSAGE);
            
        }
        
    }
    
    
    
    public int cariData() {
        int indeks = -1;
            
        if ( !getArraySoal().isEmpty() ){
            try {
                Object[] options = new Object[ getArraySoal().size() ];
                
                for ( int i = 0; i < getArraySoal().size(); i++ ){
                    options[i] = Integer.toString(i+1);
                }
                
                String inputSoal = (String) JOptionPane.showInputDialog(null, lihatSoalPanel(), "Cari Soal", JOptionPane.QUESTION_MESSAGE, null, options, "");
                indeks = Integer.parseInt(inputSoal);
                
            } catch ( NumberFormatException e){
                return -1;
            }
        
            if ( indeks != -1 ){
                return indeks;
                
            } else {
                return -1;
            }
        } else {
            JOptionPane.showMessageDialog(null, "Data Soal Kosong", "Daftar Soal Tes", JOptionPane.INFORMATION_MESSAGE);
            return -1;
        }
        
    }
    
    @Override
    public void updateData() {
        int indeks = cariData();
        if( indeks > 0 && indeks <= getArraySoal().size() ){                
            updateData(indeks-1);
            
        }
    }
    
    public void updateData(int indeks) {
        
        Object[] options = { "a", "b", "c", "d", "e"};
        try{
            String ubahJawabanBenar = (String)JOptionPane.showInputDialog(null,tambahUbahPanel(), "Ubah Soal Tes", 
                JOptionPane.QUESTION_MESSAGE, null, options, "");
            
            if (    ubahJawabanBenar.equals("a") || ubahJawabanBenar.equals("b") || 
                    ubahJawabanBenar.equals("c") || ubahJawabanBenar.equals("d") || 
                    ubahJawabanBenar.equals("e") ){
                
                if ( inputQuestion.getText().isEmpty() ||
                    inputJawaban_1.getText().isEmpty() ||
                    inputJawaban_2.getText().isEmpty() ||
                    inputJawaban_3.getText().isEmpty() ||
                    inputJawaban_4.getText().isEmpty() ||
                    inputJawaban_5.getText().isEmpty() ){
                        JOptionPane.showMessageDialog(null, "Input tidak dapat kosong","Ubah Soal Tes", JOptionPane.WARNING_MESSAGE); 
                
            
                } else {
                    getArrayIndex(indeks).setQuestion( inputQuestion.getText());
                    getArrayIndex(indeks).setJawaban_1( inputJawaban_1.getText() );
                    getArrayIndex(indeks).setJawaban_2( inputJawaban_2.getText() );
                    getArrayIndex(indeks).setJawaban_3( inputJawaban_3.getText() );
                    getArrayIndex(indeks).setJawaban_4( inputJawaban_4.getText() );
                    getArrayIndex(indeks).setJawaban_5( inputJawaban_5.getText() );
                    getArrayIndex(indeks).setJawabanBenar( ubahJawabanBenar );

                    JOptionPane.showMessageDialog(null, "Perubahan data sukses", "Ubah Soal Tes", JOptionPane.INFORMATION_MESSAGE); 
                }
            }
        } catch(NullPointerException e){
        }

    }

    @Override
    public void hapusData() {
        int indeks = cariData();
        if( indeks > 0 && indeks <= getArraySoal().size() ){    
            JOptionPane.showMessageDialog(null, "Soal tes no. " + indeks +  " berhasil dihapus",
                    "Hapus Soal Tes", JOptionPane.INFORMATION_MESSAGE);             
            getArraySoal().remove(indeks-1);
            
            if (!getArraySoal().isEmpty()){
                float nilai = (float) (100.0 / getArraySoal().size());
                for (int i = 0; i < getArraySoal().size(); i++){
                    getArrayIndex(i).setNilai(nilai);                    
                }
            }
        } 
        
    }

    
    
    
}
