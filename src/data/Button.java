package data;
import soal.TestCpns;
import javax.swing.JButton;


public class Button {
    public static JButton nextTesSoal = new JButton("Soal Selanjutnya");
    public static JButton previousTesSoal = new JButton("Soal Sebelumnya");
    
    
    public Button(){
        nextTesSoal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if ( TestCpns.getIndex() != DataSoal.getArraySoal().size() - 1 ){
                    int index = TestCpns.getIndex() +1;
                    TestCpns.setIndex( index );
                    
                    
                    TestCpns.setPertanyaan( Integer.toString(index+1) +". "+ DataSoal.getArrayIndex(index).getQuestion() );

                    TestCpns.setJawabanNo1("    a. " + DataSoal.getArrayIndex(index).getJawaban_1() ) ;
                    
                    TestCpns.setJawabanNo2( "    b. " + DataSoal.getArrayIndex(index).getJawaban_2() ) ;

                    TestCpns.setJawabanNo3( "    c. " + DataSoal.getArrayIndex(index).getJawaban_3() );
                    TestCpns.setJawabanNo4( "    d. " + DataSoal.getArrayIndex(index).getJawaban_4() );
                    TestCpns.setJawabanNo5( "    e. " + DataSoal.getArrayIndex(index).getJawaban_5() );
                    TestCpns.setJawabanPendaftar("    Jawaban anda: " + TestCpns.getArrayIndex(index) );

                }
            }
        });

        previousTesSoal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if (TestCpns.getIndex() != 0 ){

                    int index = TestCpns.getIndex() - 1;
                    TestCpns.setIndex( index  );
                    
                    
                    TestCpns.setPertanyaan( Integer.toString(index+1) +". "+ DataSoal.getArrayIndex(index).getQuestion() );

                    TestCpns.setJawabanNo1("    a. " + DataSoal.getArrayIndex(index).getJawaban_1() ) ;
                    
                    TestCpns.setJawabanNo2( "    b. " + DataSoal.getArrayIndex(index).getJawaban_2() ) ;

                    TestCpns.setJawabanNo3( "    c. " + DataSoal.getArrayIndex(index).getJawaban_3() );
                    TestCpns.setJawabanNo4( "    d. " + DataSoal.getArrayIndex(index).getJawaban_4() );
                    TestCpns.setJawabanNo5( "    e. " + DataSoal.getArrayIndex(index).getJawaban_5() );
                    TestCpns.setJawabanPendaftar("    Jawaban anda: " + TestCpns.getArrayIndex(index) );

                }
            }
        });
    }
}
