package data;
public class Pendaftar {
    
    private String nama, password;
    private int nik, noKk, tanggal, bulan, tahun,
            kesempatan_ubah_data;

    public Pendaftar(
            String nama, int nik, int noKk, 
            int tanggal, int bulan, int tahun, 
            String password, int kesempatan_ubah_data) {
        
        this.nama = nama;
        this.nik = nik;
        this.noKk = noKk;
        this.tanggal = tanggal;
        this.bulan = bulan;
        this.tahun = tahun;
        this.password = password;
        this.kesempatan_ubah_data = kesempatan_ubah_data;
        
    }

    
    
    
    //SETTER
    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setNik(int nik) {
        this.nik = nik;
    }

    public void setNoKk(int noKk) {
        this.noKk = noKk;
    }

    public void setTanggal(int tanggal) {
        this.tanggal = tanggal;
    }

    public void setBulan(int bulan) {
        this.bulan = bulan;
    }

    public void setTahun(int tahun) {
        this.tahun = tahun;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setKesempatan_ubah_data(int kesempatan_ubah_data) {
        this.kesempatan_ubah_data = kesempatan_ubah_data;
    }
    
    
    
    
    
    //GETTER
    public String getNama() {
        return nama;
    }

    public int getNik() {
        return nik;
    }

    public int getNoKk() {
        return noKk;
    }

    public int getTanggal() {
        return tanggal;
    }

    public int getBulan() {
        return bulan;
    }

    public int getTahun() {
        return tahun;
    }

    public String getPassword() {
        return password;
    }

    public int getKesempatan_ubah_data() {
        return kesempatan_ubah_data;
    }
    
    
}
