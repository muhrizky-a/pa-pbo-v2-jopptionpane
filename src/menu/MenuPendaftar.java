package menu;

import data.DataJawaban;
import data.DataPendaftar;
import java.awt.GridLayout;
import soal.TestCpns;

import java.util.Scanner;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;



public class MenuPendaftar extends Menu{
    
    Menu menuAwal;
    DataPendaftar dataAkun = new DataPendaftar();
    
    int indeks = 0;
    Scanner input = new Scanner(System.in);
    
    public MenuPendaftar() {
        this.tampilkanMenu();
    }
    
    
    void daftarAkun(){
        menuAwal = new Menu();
        if ( TestCpns.isBukaTes() == true ){
            JOptionPane.showMessageDialog(null, "Masa Pendaftaran Telah Ditutup", 
                    "Daftar Akun Pendaftar", JOptionPane.INFORMATION_MESSAGE); 
        } else {
            JPanel daftarPanel = new JPanel(new GridLayout(9, 2,10,5));
            JTextField nama= new JTextField();
            JTextField nik = new JTextField(); 
            JTextField noKk = new JTextField(); 
            JTextField tanggal = new JTextField(); 
            JTextField bulan = new JTextField();
            JTextField tahun = new JTextField();

            JPasswordField password = new JPasswordField();

            daftarPanel.add(new JLabel("Nama Lengkap: "));
            daftarPanel.add( nama );

            daftarPanel.add(new JLabel("NIK: "));
            daftarPanel.add( nik );

            daftarPanel.add(new JLabel("No. KK: "));
            daftarPanel.add( noKk );

            daftarPanel.add(new JLabel("Tanggal Lahir: "));
            daftarPanel.add( tanggal );

            daftarPanel.add(new JLabel("Bulan Lahir: "));
            daftarPanel.add( bulan );

            daftarPanel.add(new JLabel("Tahun Lahir: "));
            daftarPanel.add( tahun );

            daftarPanel.add(new JLabel("Buat Password Akun: "));
            daftarPanel.add( password );
            try {
                int login = JOptionPane.showConfirmDialog(null, daftarPanel, "Daftar Akun Pendaftar", 2, 3);
                if ( login == JOptionPane.OK_OPTION ){
                    if ( nama.getText().isEmpty() || password.getText().isEmpty()){
                        JOptionPane.showMessageDialog(null, "Nama / Password tidak boleh kosong", 
                                "ERROR", JOptionPane.WARNING_MESSAGE); 
                    } else {
                        
                        int nikInt = Integer.parseInt( nik.getText() );
                        int noKkInt = Integer.parseInt( noKk.getText()); 
                        int tanggalInt = Integer.parseInt( tanggal.getText());
                        int bulanInt = Integer.parseInt( bulan.getText());
                        int tahunInt = Integer.parseInt( tahun.getText());
                        
                        DataPendaftar.tambahData( 
                            nama.getText(), 
                            nikInt, 
                            noKkInt,
                            tanggalInt, 
                            bulanInt, 
                            tahunInt, 

                            password.getText(), 1);

                        JOptionPane.showMessageDialog(null, "Pendaftaran akun sukses", "Daftar Akun Pendaftar", JOptionPane.INFORMATION_MESSAGE); 

                    }
                        
                    

                }
            } catch ( NumberFormatException e ){
                JOptionPane.showMessageDialog(null, 
                        "NIK / No. KK / Tanggal / Bulan / Tahun harus berupa angka", 
                        "ERROR", JOptionPane.ERROR_MESSAGE);
            }
        }
         menuAwal.tampilkanMenu();
    }
    
    void login(){
        
        menuAwal = new Menu();
        
        boolean loginLoop = true, loginStatus = false;
        while ( loginLoop ){
            JPanel loginPanel = new JPanel(new GridLayout(3, 2,10,5));
            JTextField id = new JTextField();
            JPasswordField pass = new JPasswordField();
            
            loginPanel.add(new JLabel("NIK: "));
            loginPanel.add(id);
            loginPanel.add(new JLabel("Password: "));
            loginPanel.add(pass);
            try{
                int login = JOptionPane.showConfirmDialog(null, loginPanel, "Login", 
                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
                if ( login == JOptionPane.OK_OPTION ){
                    for (int i=0; i< DataPendaftar.getArrayPendaftar().size();i++){
                        if ( Integer.parseInt( id.getText()) == DataPendaftar.getArrayIndex(i).getNik() && 
                             DataPendaftar.getArrayIndex(i).getPassword().equals( pass.getText() )) {
                            indeks = i;
                            loginStatus = true;
                        }
                    }

                    if( loginStatus == true ){
                        loginLoop = false;
                        JOptionPane.showMessageDialog(null, "Login berhasil","Pesan Login", JOptionPane.INFORMATION_MESSAGE); 
                        tampilkanMenu( indeks );
                    } else {
                        JOptionPane.showMessageDialog(null, "NIK / Password anda salah","Pesan Login", JOptionPane.WARNING_MESSAGE); 
                    }

                }else if ( login == JOptionPane.CANCEL_OPTION || login == JOptionPane.DEFAULT_OPTION ){
                    loginLoop = false;
                    menuAwal.tampilkanMenu();
                }
            
            } catch ( NumberFormatException e ){
                JOptionPane.showMessageDialog(null, "NIK / Password anda salah","Pesan Login", JOptionPane.WARNING_MESSAGE); 
            }
	}
    }
    
    void lupaPassword(){
        
        boolean resetPassword = false;
        
        JPanel lupaPwPanel = new JPanel(new GridLayout(4, 2,10,5));
        JTextField nik = new JTextField(); 
        JTextField tanggal = new JTextField(); 
        JTextField bulan = new JTextField();
        JTextField tahun = new JTextField();


        lupaPwPanel.add(new JLabel("NIK: "));
        lupaPwPanel.add( nik );
        
        lupaPwPanel.add(new JLabel("Tanggal Lahir: "));
        lupaPwPanel.add( tanggal );
        
        lupaPwPanel.add(new JLabel("Bulan Lahir: "));
        lupaPwPanel.add( bulan );
        
        lupaPwPanel.add(new JLabel("Tahun Lahir: "));
        lupaPwPanel.add( tahun );
        
        try{
            int forgot = JOptionPane.showConfirmDialog(null, lupaPwPanel, "Lupa Password?", 
                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);

            if ( forgot == JOptionPane.OK_OPTION ){
                for (int i=0; i< DataPendaftar.getArrayPendaftar().size();i++){
                    if ( 
                        Integer.parseInt( nik.getText())     == DataPendaftar.getArrayIndex(i).getNik() && 
                        Integer.parseInt( tanggal.getText()) == DataPendaftar.getArrayIndex(i).getTanggal() &&
                        Integer.parseInt( bulan.getText())   == DataPendaftar.getArrayIndex(i).getBulan() &&
                        Integer.parseInt( tahun.getText())   == DataPendaftar.getArrayIndex(i).getTahun() ) {
                        indeks = i;
                        resetPassword = true;
                    }
                }
                if( resetPassword == true ){

                    JPanel resetPwPanel = new JPanel(new GridLayout(5, 1,10,5));
                    JPasswordField password = new JPasswordField();

                    resetPwPanel.add(new JLabel("Data ditemukan"));
                    resetPwPanel.add(new JLabel( "Nama: " + DataPendaftar.getArrayIndex(indeks).getNama() ));
                    resetPwPanel.add(new JLabel( "NIK : " + DataPendaftar.getArrayIndex(indeks).getNik() ));

                    resetPwPanel.add(new JLabel("Buat Password Baru: "));
                    resetPwPanel.add( password );

                    int reset = JOptionPane.showConfirmDialog(null, resetPwPanel, "Reset Password", 
                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);

                    if ( reset == JOptionPane.OK_OPTION ){
                        if ( password.getText().isEmpty()){
                            JOptionPane.showMessageDialog(null, "Password tidak boleh kosong", "ERROR", JOptionPane.WARNING_MESSAGE); 
                        } else {
                            DataPendaftar.getArrayIndex(indeks).setPassword( password.getText() );
                            JOptionPane.showMessageDialog(null, "Perubahan password sukses", "Reset Password", JOptionPane.INFORMATION_MESSAGE); 
                        }
                    }


                }else{
                    JOptionPane.showMessageDialog(null, "NIK / tanggal lahir tidak cocok", 
                            "Lupa Password?", JOptionPane.WARNING_MESSAGE); 
                }
            }
        }catch ( NumberFormatException e ){
            JOptionPane.showMessageDialog(null, 
                    "NIK / No. KK / Tanggal / Bulan / Tahun harus berupa angka", 
                    "ERROR", JOptionPane.ERROR_MESSAGE);
            
        }
        
        tampilkanMenu();
    }
    
    @Override
    public void tampilkanMenu(){
        JPanel pendaftarPanel = new JPanel(new GridLayout(6, 1,10,5));
        pendaftarPanel.add(new JLabel("1. Daftar Akun"));
        pendaftarPanel.add(new JLabel("2. Login Akun"));
        pendaftarPanel.add(new JLabel("3. Lupa Password?"));
        pendaftarPanel.add(new JLabel("4. Kembali ke menu awal"));
        pendaftarPanel.add(new JLabel(""));
        pendaftarPanel.add(new JLabel("Masukan Pilihan:"));
        
        boolean loop = true;
        while(loop){
            try{
                Object[] options = { "1.", "2.", "3.", "4."};

            
                String opsi = (String)JOptionPane.showInputDialog( null, pendaftarPanel, "Menu Admin", JOptionPane.QUESTION_MESSAGE, null, options, "");

                switch (opsi) {
                    case "1.":
                        loop = false;
                        daftarAkun();
                        break;

                    case "2.":
                        loop = false;
                        login();
                        break;

                    case "3.":
                        loop = false;
                        lupaPassword();
                        break;

                    case "4.":
                        loop = false;
                        menuAwal = new Menu();
                        menuAwal.tampilkanMenu();
                        break;

                }
            } catch(NullPointerException e){
                JOptionPane.showMessageDialog(null, "Pilihan Tidak ada","ERROR", JOptionPane.ERROR_MESSAGE); 

            }
        }
    }
    
    public void tampilkanMenu( int indeks ){
        
        JPanel menuPendaftar = new JPanel(new GridLayout(6, 1,10,5));
        
        if(DataJawaban.getArrayIndex(indeks).isTelahIkutTes() == true){
            menuPendaftar.add(new JLabel("1. Hasil Seleksi"));
        } else {
            menuPendaftar.add(new JLabel("1. Mulai Tes"));
        }
        
        menuPendaftar.add(new JLabel("2. Lihat Data Akun"));
        menuPendaftar.add(new JLabel("3. Ubah Data Akun"));
        menuPendaftar.add(new JLabel("4. Kembali ke menu awal"));
        menuPendaftar.add(new JLabel(""));
        menuPendaftar.add(new JLabel("Masukan Pilihan:"));
        
        boolean loop = true;
        while(loop){
            try{
                Object[] options = { "1.", "2.", "3.", "4."};

            
                String opsi = (String)JOptionPane.showInputDialog( 
                        null, menuPendaftar, 
                        "Selamat Datang, " + DataPendaftar.getArrayIndex(indeks).getNama(), 
                        JOptionPane.QUESTION_MESSAGE, null, options, "");

                switch (opsi) {
                    case "1.":
                        loop = false;
                        
                        
                        if(DataJawaban.getArrayIndex(indeks).isTelahIkutTes() == true){
                            TestCpns.lihatHasilSeleksi(indeks);
                            tampilkanMenu(indeks);
                        } else {
                            TestCpns.mulaiTes(indeks);
                            tampilkanMenu(indeks);
                        }

                        
                        break;

                    case "2.":
                        loop = false;
                        dataAkun.lihatData(indeks);
                        tampilkanMenu(indeks);
                        break;

                    case "3.":
                        loop = false;
                        int kesempatan_ubah = DataPendaftar.getArrayIndex(indeks).getKesempatan_ubah_data();
                        if ( kesempatan_ubah == 0 || TestCpns.isBukaTes() == true){
                            
                            JPanel ubahPanel = new JPanel(new GridLayout(4, 1,10,5));
                            ubahPanel.add(new JLabel("  Kesempatan ubah data telah habis  "));
                            ubahPanel.add(new JLabel("                ATAU                "));
                            ubahPanel.add(new JLabel("       Sesi Tes Telah Dibuka.       "));
                            ubahPanel.add(new JLabel("Data Tidak Dapat Diubah - Ubah Lagi."));
                            
                            JOptionPane.showMessageDialog(null, ubahPanel, "Ubah Data", JOptionPane.INFORMATION_MESSAGE); 
                            
                        } else {
                            JOptionPane.showMessageDialog(null, "Anda hanya dapat mengubah data 1 kali", "Ubah Data", JOptionPane.INFORMATION_MESSAGE); 
                            dataAkun.updateData(indeks);
                        }

                        tampilkanMenu(indeks);
                        break;

                    case "4.":
                        loop = false;
                        menuAwal = new Menu();
                        menuAwal.tampilkanMenu();
                        break;

                }
            } catch(NullPointerException e){
                JOptionPane.showMessageDialog(null, "Pilihan Tidak ada","ERROR", JOptionPane.ERROR_MESSAGE); 

            }
        }
    }
    
}
